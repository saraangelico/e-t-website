mlcc
----

Keywords specific to multilevel coupled cluster enter into the ``mlcc`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

	``levels: [string], [string], ...``

	Specifies the level of theory for the different active spaces. Required keyword.

	Valid keyword values are:

   - ``ccs``
   - ``cc2``
   - ``ccsd``

   .. note::
      Necessary for MLCCSD calculation.
      
.. container:: sphinx-custom

	``cc2 orbitals: [string]``

	Specifies the type of orbitals used for orbital partitioning in MLCC2. Required keyword.

	Valid keyword values are:

	- ``cnto-approx`` Approximated correlated natural transition orbitals
	- ``cholesky`` Cholesky orbitals (occupied and virtual)
	- ``cholesky-pao`` Cholesky occupied orbitals and projected atomic orbitals for the virtuals.
	- ``nto-canonical`` Natural transition orbitals for occupied and canonical for virtuals.

	.. warning::
		``nto-canonical`` and ``cholesky`` orbitals are not generally recommended.
      
.. container:: sphinx-custom

	``ccsd orbitals: [string]``

	Specifies the type of orbitals used for orbital partitioning in MLCCSD. Required keyword.

	Valid keyword values are:

	- ``cnto-approx`` Approximated correlated natural transition orbitals
	- ``cholesky`` Cholesky orbitals (occupied and virtual)
	- ``cholesky-pao`` Cholesky occupied orbitals and projected atomic orbitals for the virtuals.
	- ``nto-canonical`` Natural transition orbitals for occupied and canonical for virtuals.

	.. warning::
		``nto-canonical`` and ``cholesky`` orbitals are not generally recommended.

.. container:: sphinx-custom

	``cholesky threshold: [real]``

	Default:
	:math:`1.0\cdot 10^{-2}` (or 1.0d-2)

	Threshold for the Cholesky decomposition of the density. Only used with ``cc2 orbitals: cholesky`` or ``cc2 orbitals: cholesky-pao``

.. container:: sphinx-custom

   ``cnto occupied cc2: [integer]``

   Determines the number of occupied orbitals in the CC2 orbital space for the MLCC2 calculation. 

   .. note::
      Necessary if ``cc2 orbitals: cnto-approx`` is given.

.. container:: sphinx-custom

   ``cnto virtual cc2: [integer]``

   Default: :math:`n_v^{\text{CC2}} = n_o^{\text{CC2}}\cdot\frac{n_v}{n_o}`

   Sets the number of virtual orbitals in the CC2 orbital space for the MLCC2 calculation. Optional.

   .. note::
      Only used if ``cc2 orbitals: cnto-approx`` is given.

.. container:: sphinx-custom

   ``cnto occupied ccsd: [integer]``

   Determines the number of occupied orbitals in the CCSD orbital space for the MLCCSD calculation. 

   .. note::
      Necessary if ``ccsd orbitals: cnto-approx`` is given.

.. container:: sphinx-custom

   ``cnto virtual ccsd: [integer]``

   Default: :math:`n_v^{\text{CCSD}} = n_o^{\text{CCSD}}\cdot\frac{n_v}{n_o}`

   Sets the number of virtual orbitals in the CCSD orbital space for the MLCCSD calculation. Optional.

   .. note::
      Only used if ``ccsd orbitals: cnto-approx`` is given.

.. container:: sphinx-custom

   ``cnto states: {[integer], [integer], ...}``

   Determines which CCS excited states are used to construct the approximated CNTOs.

   .. note::
      Necessary if ``cc2 orbitals: cnto-approx`` or ``ccsd orbitals: cnto-approx`` is given.

.. container:: sphinx-custom

   ``print ccs calculation``

   Default: false

   Enables the printing of the CCS calculation in the case of ``cc2 orbitals: cnto-approx`` 
   or ``ccsd orbitals: cnto-approx``. Optional. 

.. container:: sphinx-custom

   ``print cc2 calculation``

   Default: false

   Enables the printing of the CC2 calculation in the case of ``ccsd orbitals: cnto``. Optional. 

.. container:: sphinx-custom

   ``nto occupied cc2: [integer]``

   Determines the number of occupied orbitals in the CC2 orbital space for the MLCC2 calculation. 

   .. note::
      Necessary if ``cc2 orbitals: nto-canonical`` is given.

.. container:: sphinx-custom

   ``nto occupied ccsd: [integer]``

   Determines the number of occupied orbitals in the CCSD orbital space for the MLCCSD calculation. 

   .. note::
      Necessary if ``ccsd orbitals: nto-canonical`` is given.
            
.. container:: sphinx-custom

   ``nto virtual cc2: [integer]``

   Determines the number of virtual orbitals in the CC2 orbital space for the MLCC2 calculation. Optional.

   .. note::
      Only used if ``cc2 orbitals: nto-canonical`` is given.

.. container:: sphinx-custom

   ``canonical virtual cc2: [integer]``

   Default: :math:`n_v^{\text{CC2}} = n_o^{\text{CC2}}\cdot\frac{n_v}{n_o}`

   Sets the number of virtual orbitals in the CC2 orbital space for the MLCC2 calculation. Optional.

   .. note::
      Only used if ``cc2 orbitals: nto-canonical`` is given.

.. container:: sphinx-custom

   ``canonical virtual ccsd: [integer]``

   Default: :math:`n_v^{\text{CCSD}} = n_o^{\text{CCSD}}\cdot\frac{n_v}{n_o}`

   Sets the number of virtual orbitals in the CCSD orbital space for the MLCCSD calculation. Optional.

   .. note::
      Only used if ``ccsd orbitals: nto-canonical`` is given.

.. container:: sphinx-custom

   ``nto states: {[integer], [integer], ...}``

   Determines which CCS excited states are used to construct the NTOs.

   .. note::
      Necessary if ``cc2 orbitals: nto-canonical`` is given.

.. container:: sphinx-custom

   ``cnto restart``

   Default: false

   If specified, the solver will attempt to restart from previously determined CNTO matrices (M and N). Optional.

.. container:: sphinx-custom

   ``orbital restart``

   Default: false

   If specified, orbital partitioning is skipped and MLCC orbitals (and sizes of orbital sets) are read from file. Optional.


cc response
-----------
Keywords specific to response calculations are given in the ``cc response`` section.

For **EOM** transition moments from ground and excited states,
and permanent moments for excited states are implemented for CCS, CC2, CCSD and CC3.
**EOM** polarizabilities are implemented for CCS, CC2 and CCSD.

For **linear response** transition moments from the ground state
and polarizabilities are implemented for CCS, CC2, and CCSD.

.. note::
   This section is required if the keyword ``response`` is given in the ``do`` section.

   One of the keywords ``polarizabilities``, ``permanent moments`` or ``transition moments`` must be specified.

Keywords
^^^^^^^^
.. container:: sphinx-custom

   ``polarizabilities``

   Enables the calculation of polarizabilities.

.. container:: sphinx-custom

   ``transition moments``

   Enables the calculation of transition moments.

.. container:: sphinx-custom

   ``eom``

   Properties will be calculated within the equation of motion formalism.

   Either this or the ``lr`` keyword must be specified.

   Available for CCS, CC2, CCSD, and CC3.

.. container:: sphinx-custom

   ``lr``

   Properties will be calculated within the linear response formalism.

   Either this or the ``eom`` keyword must be specified.

   Available for CCS, CC2, and CCSD.

.. container:: sphinx-custom

   ``dipole length``

   Required keyword. Currently the only operator available for response calculations in :math:`e^T`.

.. container:: sphinx-custom

   ``frequencies: {[real], [real], ...}``

   Frequencies, in Hartree, for which the polarizability shall be computed.
   Required for polarizabilities.

.. container:: sphinx-custom

   ``initial states: {[real], [real], ...}``

   Default: ``{0}`` (Only the ground state is considered.)

   Numbers of the states for which the transition/permanent moments shall be computed.

.. container:: sphinx-custom

   ``permanent moments``

   Enables the calculation of permanent moments.

   .. note::
      Only implemented in the EOM formalism for excited states.

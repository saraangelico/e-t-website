hf mean value
-------------

The ``hf mean value`` section is used to obtain HF expectation values. 

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``dipole``

   Calculation of the HF dipole moment.

.. container:: sphinx-custom

   ``quadrupole``

   Calculation of the HF quadrupole moment. 

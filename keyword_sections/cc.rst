cc
--

General coupled cluster keywords.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``Bath orbital``

   Default: false

   Add a bath orbital to the calculation with zero orbital energy 
   and zero electron repulsion integrals.
   
   .. note::
      This keyword is required to compute ionized states.

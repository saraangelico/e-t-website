cc mean value
-------------

The ``cc mean value`` section is used to obtain CC ground state expectation values. 

.. note::
   This section is required if the keyword ``mean value`` is given in the ``do`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``dipole``

   Calculation of coupled cluster ground state dipole moment.

.. container:: sphinx-custom

   ``quadrupole``

   Calculation of coupled cluster ground state quadrupole moment.

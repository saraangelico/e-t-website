fci mean value
--------------

The ``fci mean value`` section is used to obtain expectation values
of operators for the FCI ground state.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``dipole``

   Calculation of the FCI ground state dipole moment.

.. container:: sphinx-custom

   ``quadrupole``

   Calculation of the FCI ground state quadrupole moment. 

This is the repository for the [website](https://etprogram.org/) of the eT program. For the code repository for the program, see the main eT repository [here](https://gitlab.com/eT-program/eT).

# Prerequisites
1. [Python](https://www.python.org/)
2. [Sphinx](https://www.sphinx-doc.org/en/master/)

The Sphinx python package can be installed using `pip`:
```shell
pip install sphinx==4.5.0
```
Make sure to specify the version of Sphinx, we have had issues with the later versions. E.g., Sphinx v6.0.0 does not work.

# Quick build
Clone the repository with SSH or HTTPS:
```shell
git clone git@gitlab.com:eT-program/website.git et-website
git clone https://gitlab.com/eT-program/website.git et-website
```
Enter the main folder and build:
```shell
cd et-website
make html
```
If the build succeeds, you can check out the website by opening `_build/html/index.html` in your browser.
